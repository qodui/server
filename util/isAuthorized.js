const mongo = require("../db/mongo");
const constants = require("../routes/constants");


function isAuthorized(authId) {
	return new Promise(async (resolve, reject) => {
		try {
			const sessions = await mongo.get(constants.SECURITY_DB, constants.SESSION_COLLECTION, {
				authId
			});
			if (sessions.length === 1) {
				resolve(true);
			} else {
				resolve(false);
			}
		} catch (error) {
			reject(error);
		}
	});
}

module.exports = isAuthorized;
