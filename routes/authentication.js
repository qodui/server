const express = require("express");
const bcrypt = require("bcrypt");
const _ = require("lodash");
const uuid = require("uuid/v1");
var { ObjectId } = require("mongodb");
const mongo = require("../db/mongo");
const router = express.Router();
const constants = require("./constants");

router.post("/login", async (req, res, next) => {
	try {
		const users = await mongo.get(constants.SECURITY_DB, constants.USER_COLLECTION, {
			userId: req.body.userId
		});
		if (users && users.length === 1) {
			const valid = bcrypt.compareSync(
				req.body.password,
				users[0].password
			);
			if (valid) {
				const authId = uuid();
				await mongo.insert(constants.SECURITY_DB, constants.SESSION_COLLECTION, {
					userId: req.body.userId,
					authId
				});
				let response = _.omit(users[0], "password");
				let role = await mongo.get(constants.SECURITY_DB, constants.ROLES_COLLECTION, {
					name: response.role
				});
				let capabilities = role[0].capabilities.map(c => ObjectId(c));
				let userCapabilities = await mongo.get(
					constants.SECURITY_DB,
					constants.CAPABILITIES_COLLECTION,
					{ _id: { $in: capabilities } }
				);
				response = {
					...response,
					authId,
					capabilities: userCapabilities
				};
				res.send(response);
			} else {
				res.status(403).send({
					message: "The entered password is wrong."
				});
			}
		} else {
			res.status(403).send({
				message:
					"The entered user ID does not exist. Please contact the admin for credentials."
			});
		}
	} catch (error) {
		error.custom = "Failed to authenticate user due to ";
		next(error);
	}
});

router.post("/logout", async (req, res, next) => {
	try {
		let { userId, authId } = req.body;
		await mongo.delete(constants.SECURITY_DB, constants.SESSION_COLLECTION, { userId, authId });
		res.send({ message: "Logout successful." });
	} catch (error) {
		error.custom = "Failed to logout user due to ";
		next(error);
	}
});

module.exports = router;
