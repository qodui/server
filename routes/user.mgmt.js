const express = require("express");
const bcrypt = require("bcrypt");
var { ObjectId } = require("mongodb");

const mongo = require("../db/mongo");
const User = require("../misc/User");
const isAuthorized = require("../util/isAuthorized");
const router = express.Router();
const BCRYPT_SALT_ROUNDS = 12;
const constants = require("../routes/constants");


router.get("/users", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const users = await mongo.get(constants.SECURITY_DB, constants.USER_COLLECTION, {});
			res.send(users);
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Could not get all users due to error ";
		next(error);
	}
});

router.get("/user/:id", async (req, res, next) => {
	try {
		const users = await mongo.get(constants.SECURITY_DB, constants.USER_COLLECTION, {
			_id: ObjectId(req.params.id)
		});
		if (users.length === 1) {
			res.send(users[0]);
		} else {
			res.status(409).send({ message: "Failed to fetch user details." });
		}
	} catch (error) {
		error.custom = "Could not get all user info due to error ";
		next(error);
	}
});

router.post("/addUser", async (req, res, next) => {
	try {
		let { userId, password, name, role } = req.body;
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const users = await mongo.get(constants.SECURITY_DB, constants.USER_COLLECTION, { userId });
			if (users.length === 0) {
				let hashedPassword = bcrypt.hashSync(
					password,
					BCRYPT_SALT_ROUNDS
				);
				const insertedDocument = await mongo.insert(
					DB,
					USERS_COLLECTION,
					new User(userId, name, hashedPassword, role)
				);
				res.send(insertedDocument.ops[0]);
			} else {
				res.status(409).send({
					message: `The User ${req.body.userId} is already present in the system.`
				});
			}
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to add user due to error ";
		next(error);
	}
});

router.put("/editUser/:id", async (req, res, next) => {
	try {
		let hashedPassword = req.body.password
			? bcrypt.hashSync(req.body.password, BCRYPT_SALT_ROUNDS)
			: undefined;
		if (hashedPassword) req.body.password = hashedPassword;
		const authorized = isAuthorized(req.headers.authid);
		if (authorized) {
			const users = await mongo.get(constants.SECURITY_DB, constants.USER_COLLECTION, {
				userId: req.body.userId
			});
			if (
				(users.length === 1 && users[0]._id == req.params.id) ||
				users.length === 0
			) {
				await mongo.update(
					constants.SECURITY_DB,
					constants.USER_COLLECTION,
					{ _id: ObjectId(req.params.id) },
					req.body
				);
				const updatedUsers = await mongo.get(constants.SECURITY_DB, constants.USER_COLLECTION, {
					userId: req.body.userId
				});
				res.send(updatedUsers[0]);
			} else {
				res.status(409).send({
					message: `The user ${req.body.userId} is already present in the system.`
				});
			}
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to update user due to error ";
		next(error);
	}
});

router.delete("/deleteUser/:id", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			await mongo.delete(constants.SECURITY_DB, constants.USER_COLLECTION, {
				_id: ObjectId(req.params.id)
			});
			res.send({ message: "Deleted the user successfuly." });
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to delete user due to error ";
		next(error);
	}
});

module.exports = router;
