const express = require("express");
const mongo = require("../db/mongo");
const isAuthorized = require("../util/isAuthorized");
const router = express.Router();
const middleware = require("./middleware");
const constants = require("../routes/constants");


router.get("/id/:id", async (req, res, next) => {
  try {
    const authorized = await isAuthorized(req.headers.authid);
    if (authorized) {
      const execution = await mongo.get(constants.QOD_DB, constants.EXECUTIONS_COLLECTION, {
        _id: req.params.id
      });
      res.send(execution);
    } else {
      res.status(403).send({
        message: "Unauthorized to perform this operation."
      });
    }
  } catch (error) {
    error.custom = "Failed to fetch executions due to error ";
    next(error);
  }
});

router.get("/all", async (req, res, next) => {
  const purpose = req.query.purpose;
  console.log(`execution/all/?purpose=${purpose}`);
  try {
    const authorized = await isAuthorized(req.headers.authid);
    let filterQuery = {};
    if (authorized) {
      if (purpose === "executions") {
        filterQuery = {
          status: {
            $in: ["started", "running", "pending", "waiting","yet to start"]
          }
        };
      }
      else if (purpose === "reports") {
        filterQuery = {
          status: {
            $in: ["failed","successful","not started"]
          }
        };
      }
      const executions = await mongo.get(constants.QOD_DB, constants.EXECUTIONS_COLLECTION, filterQuery, {created: -1});
      //console.log(`executions: ${JSON.stringify(executions, null, 2)}`)
      res.send(executions);
    } else {
      res.status(403).send({
        message: "Unauthorized to perform this operation."
      });
    }
  } catch (error) {
    error.custom = "Failed to fetch executions due to error ";
    next(error);
  }
});

router.post("/create", async (req, res, next) => {
  console.log("create execution called");
  try {
    const authId = req.headers.authid;
    const userId = req.headers.userid;
    const authorized = await isAuthorized(authId);
    let data = req.body;
    console.log(`data: ${JSON.stringify(data, null, 2)}`);
    if (authorized) {
      data.owner = userId;
      //data.status = "started";
      //data.monitorLink = "http://13.234.32.165:3000/d/qwrpbyJZk/jmeter-dashboard1?orgId=1&refresh=5s&var-data_source=Hipstershop&var-application=Weaversocks&var-transaction=Add%20to%20Cart&var-measurement_name=jmeter&var-send_interval=5";
      //console.log(`data: ${JSON.stringify(data)}`);
      let newExec = null;
      const checkExec = await mongo.get(constants.QOD_DB, constants.EXECUTIONS_COLLECTION, {
        executionName: data.executionName
      });
      if (checkExec.length === 0) {
        newExec = await mongo.insert(constants.QOD_DB, constants.EXECUTIONS_COLLECTION, data);
        console.log(`created execution doc in mongodb`);
        await middleware
          .post("/executions/create/", data, {
            headers: {
              authId: "baba6000-6781-11ea-aae6-c9e7a8a5c7d5",
              userId: userId
            }
          })
          .then(response => {
            res.status(200).send(response.data);
            return;
          })
          .catch(async function(error) {
            data = newExec.ops[0];
            data.status = "failed";
            var datetime = new Date();
            datetime = datetime.split("T")[0]+" "+datetime.split("T")[1].split(".")[0]
            data.created = datetime;
            const updatedExec = await mongo.update(
              DB,
              EXEC_COLLECTION,
              { _id: data._id },
              data
            );
            console.log("updated the status as failed");

            console.log(`error: ${JSON.stringify(error, null, 2)}`);
            if (error.response) {
              // The request was made and the server responded with a status code
              // that falls out of the range of 2xx
              console.log(`error response: ${error.response}`);
              //console.log(error.response.status);
              //console.log(error.response.headers);
            } else if (error.request) {
              // The request was made but no response was received
              // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
              // http.ClientRequest in node.js
              console.log(
                `error request: ${JSON.stringify(error.request, null, 2)}`
              );
            } else {
              // Something happened in setting up the request that triggered an Error
              console.log("Error", error.message);
            }
            res.status(500).send();
            return;
          });
      } else {
        res.status(409).send({
          message: `The execution with name "${data.executionName}" is already present.`
        });
      }
      //console.log(`response from middleware: ${JSON.stringify(middlewareResponse, null,2)}`)
      //console.log(`response from middleware: ${middlewareResponse.response.data}`)
    } else {
      res.status(403).send({
        message: "Unauthorized to perform this operation."
      });
    }
  } catch (error) {
    error.custom = `Failed to create execution due to error: ${error}`;
    console.log(`error: ${JSON.stringify(error.message, null, 2)}`);
    next(error);
  }
});

router.delete("/delete", async (req, res, next) => {
  try {
    const authorized = await isAuthorized(req.headers.authid);
    if (authorized) {
      const response = await mongo.delete(constants.QOD_DB, constants.EXECUTIONS_COLLECTION, {
        name: { $in: req.body }
      });
      res.send({
        message: `Successfully deleted ${response.deletedCount} execution(s).`
      });
    } else {
      res.status(403).send({
        message: "Unauthorized to perform this operation."
      });
    }
  } catch (error) {
    error.custom = "Failed to delete execution(s) due to error ";
    next(error);
  }
});

module.exports = router;
