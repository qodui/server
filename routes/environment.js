const express = require("express");
const mongo = require("../db/mongo");
const isAuthorized = require("../util/isAuthorized");
const router = express.Router();
const constants = require("../routes/constants");


router.get("/all", async (req, res, next) => {
  let status = req.query.status;
  let filterQuery = {};
  if (status === "free") {
    filterQuery = { status: { $in: ["To Be Created", "Pre-Existing"] } };
  }

  try {
    const authorized = await isAuthorized(req.headers.authid);
    if (authorized) {
      const environments = await mongo.get(constants.QOD_DB, constants.ENVIRONMENTS_COLLECTION, filterQuery, { created: -1 });
      res.send(environments);
    } else {
      res.status(403).send({
        message: "Unauthorized to perform this operation."
      });
    }
  } catch (error) {
    error.custom = "Failed to fetch environments due to error ";
    next(error);
  }
});

router.post("/create", async (req, res, next) => {
  try {
    const authorized = await isAuthorized(req.headers.authid);
    if (authorized) {
      const environments = await mongo.get(constants.QOD_DB, constants.ENVIRONMENTS_COLLECTION, {
        name: req.body.name
      });
      if (environments.length === 0) {
        const newEnv = await mongo.insert(constants.QOD_DB, constants.ENVIRONMENTS_COLLECTION, req.body);
        res.send(newEnv.ops[0]);
      } else {
        res.status(409).send({
          message: `The environment with name "${req.body.name}" is already present.`
        });
      }
    } else {
      res.status(403).send({
        message: "Unauthorized to perform this operation."
      });
    }
  } catch (error) {
    error.custom = "Failed to create environment due to error ";
    next(error);
  }
});

router.delete("/delete", async (req, res, next) => {
  try {
    const authorized = await isAuthorized(req.headers.authid);
    if (authorized) {
      const response = await mongo.delete(constants.QOD_DB, constants.ENVIRONMENTS_COLLECTION, {
        name: { $in: req.body }
      });
      res.send({
        message: `Successfully deleted ${response.deletedCount} environment(s).`
      });
    } else {
      res.status(403).send({
        message: "Unauthorized to perform this operation."
      });
    }
  } catch (error) {
    error.custom = "Failed to delete environment(s) due to error ";
    next(error);
  }
});

module.exports = router;
