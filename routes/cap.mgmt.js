const express = require("express");
var { ObjectId } = require("mongodb");
const mongo = require("../db/mongo");
const isAuthorized = require("../util/isAuthorized");
const router = express.Router();
const constants = require("../routes/constants");


router.get("/capabilities", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const capabilities = await mongo.get(
				constants.SECURITY_DB,
				constants.CAPABILITIES_COLLECTION,
				{}
			);
			res.send(capabilities);
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to fetch capabilities due to error ";
		next(error);
	}
});

router.post("/createCapability", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const capabilities = await mongo.get(constants.SECURITY_DB, constants.CAPABILITIES_COLLECTION, {
				name: req.body.name
			});
			if (capabilities.length === 0) {
				const insertedCapabilities = await mongo.insert(
					constants.SECURITY_DB,
					constants.CAPABILITIES_COLLECTION,
					req.body
				);
				res.send(insertedCapabilities.ops[0]);
			} else {
				res.status(409).send({
					message: `The capability ${req.body.name} is already present in the system.`
				});
			}
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to add capability due to error ";
		next(error);
	}
});

router.put("/editCapability/:id", async (req, res, next) => {
	try {
		const authorized = isAuthorized(req.headers.authid);
		if (authorized) {
			const capabilities = await mongo.get(constants.SECURITY_DB, constants.CAPABILITIES_COLLECTION, {
				name: req.body.name
			});
			if (
				(capabilities.length === 1 &&
					capabilities[0]._id == req.params.id) ||
				capabilities.length === 0
			) {
				await mongo.update(
					constants.SECURITY_DB,
					constants.CAPABILITIES_COLLECTION,
					{ _id: ObjectId(req.params.id) },
					req.body
				);
				const updatedCapability = await mongo.get(
					constants.SECURITY_DB,
					constants.CAPABILITIES_COLLECTION,
					{ name: req.body.name }
				);
				res.send(updatedCapability[0]);
			} else {
				res.status(409).send({
					message: `The capability ${req.body.name} is already present in the system.`
				});
			}
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to update capability due to error ";
		next(error);
	}
});

router.delete("/deleteCapability/:id", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			await mongo.delete(constants.SECURITY_DB, constants.CAPABILITIES_COLLECTION, {
				_id: ObjectId(req.params.id)
			});
			await mongo.update(
				constants.SECURITY_DB,
				constants.ROLES_COLLECTION,
				{},
				{ $pull: { capabilities: { $in: [req.params.id] } } }
			);
			res.send({ message: "Delete successful" });
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to delete capability due to error ";
		next(error);
	}
});

module.exports = router;
