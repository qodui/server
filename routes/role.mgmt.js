const express = require("express");
var { ObjectId } = require("mongodb");
const mongo = require("../db/mongo");
const isAuthorized = require("../util/isAuthorized");
const constants = require("../routes/constants");
const router = express.Router();


router.get("/roles", async (req, res, next) => {
	try {
		const response = await mongo.get(constants.SECURITY_DB, constants.ROLES_COLLECTION, {});
		res.send(response);
	} catch (error) {
		error.custom = "Error fetching roles ";
		next(error);
	}
});

router.post("/createRole", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const isPresent = await mongo.get(constants.SECURITY_DB, constants.ROLES_COLLECTION, {
				name: req.body.name
			});
			if (isPresent.length === 0) {
				const response = await mongo.insert(
					constants.SECURITY_DB,
					constants.ROLES_COLLECTION,
					req.body
				);
				res.send(response.ops[0]);
			} else {
				res.status(409).send({
					message: `The role ${req.body.name} is already present in the system.`
				});
			}
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "The role could not be added due to error ";
		next(error);
	}
});

router.put("/editRole/:id", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const roles = await mongo.get(constants.SECURITY_DB, constants.ROLES_COLLECTION, {
				name: req.body.name
			});
			if (
				(roles.length === 1 && roles[0]._id == req.params.id) ||
				roles.length === 0
			) {
				await mongo.update(
					constants.SECURITY_DB,
					constants.ROLES_COLLECTION,
					{ _id: ObjectId(req.params.id) },
					req.body
				);
				const response = await mongo.get(constants.SECURITY_DB, constants.ROLES_COLLECTION, {
					name: req.body.name
				});
				res.send(response[0]);
			} else {
				res.status(409).send({
					message: `The role ${req.body.name} is already present in the system.`
				});
			}
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "The role could not be updated due to an error ";
		next(error);
	}
});

router.delete("/deleteRole/:id", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const roleInfo = await mongo.get(constants.SECURITY_DB, constants.ROLES_COLLECTION, {
				_id: ObjectId(req.params.id)
			});
			const usersWithRole = await mongo.get(constants.SECURITY_DB, constants.USER_COLLECTION, {
				role: roleInfo[0].name
			});
			if (usersWithRole.length > 0) {
				res.status(409).send({
					message:
						"There are users with this role. Please change those users' role and try again."
				});
			} else {
				await mongo.delete(constants.SECURITY_DB, constants.ROLES_COLLECTION, {
					_id: ObjectId(req.params.id)
				});
				res.send({ message: "Role successfully deleted." });
			}
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "The role could not be deleted due to an error ";
		next(error);
	}
});

module.exports = router;
