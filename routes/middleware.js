const axios = require("axios");
const constants = require("./constants");

module.exports = axios.create({
	baseURL: `http://localhost:${constants.DJANGO_PORT}`
});
