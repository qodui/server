const express = require("express");
const mongo = require("../db/mongo");
const isAuthorized = require("../util/isAuthorized");
const router = express.Router();
const constants = require("../routes/constants");


router.get("/all", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const configurations = await mongo.get(constants.QOD_DB, constants.CONFIGURATIONS_COLLECTION, {}, {created: -1});
			res.send(configurations);
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to fetch configurations due to error ";
		next(error);
	}
});

router.post("/create", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const configurations = await mongo.get(constants.QOD_DB, constants.CONFIGURATIONS_COLLECTION, {
				name: req.body.name
			});
			if (configurations.length === 0) {
				const newConf = await mongo.insert(
					constants.QOD_DB,
					constants.CONFIGURATIONS_COLLECTION,
					req.body
				);
				res.send(newConf.ops[0]);
			} else {
				res.status(409).send({
					message: `The configuration with name "${req.body.name}" is already present.`
				});
			}
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to create configuration due to error ";
		next(error);
	}
});

router.delete("/delete", async (req, res, next) => {
	try {
		const authorized = await isAuthorized(req.headers.authid);
		if (authorized) {
			const response = await mongo.delete(constants.QOD_DB, constants.CONFIGURATIONS_COLLECTION, {
				name: { $in: req.body }
			});
			res.send({
				message: `Successfully deleted ${response.deletedCount} configuration(s).`
			});
		} else {
			res.status(403).send({
				message: "Unauthorized to perform this operation."
			});
		}
	} catch (error) {
		error.custom = "Failed to delete configuration(s) due to error ";
		next(error);
	}
});

module.exports = router;
