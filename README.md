# Quality Of Devices (QOD)

This is the server side of the QODUI project.

## Pre-Requisites

* VSCode IDE
* NodeJS v10.16 and above
* npm v6.9.0 and above
* Git
* Python 2.7 for node-gyp to work
* Robo3T - To connect to MongoDB and verify data
* Postman - To test the endpoints without the client
* MongoDB Community Edition - To test on a local mongodb database.

## Setup

Clone the repository to your local machine using the following command from command prompt:

```
    git clone https://<userID>@bitbucket.org/qodui/server.git
```

Run the following commands in order:

```
    npm install -g node-gyp
    npm install
```

### Available Scripts

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.<br>

The application will start listening to requests on `localhost:8080`

Avoid getting self signed certificate by running export NODE_TLS_REJECT_UNAUTHORIZED=0

