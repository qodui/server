function User(userId, name, password, role) {
	this.userId = userId;
	this.name = name;
	this.password = password;
	this.role = role;
}

module.exports = User;
