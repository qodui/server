var mongoClient = require("mongodb").MongoClient;

const CONNECTION_INFO = require("./constants");
const BASE_URL = `mongodb://${CONNECTION_INFO.user}:${CONNECTION_INFO.password}@${CONNECTION_INFO.address}:${CONNECTION_INFO.port}/${CONNECTION_INFO.db}`;
const clientOptions = {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	serverSelectionTimeoutMS: 60000
};

function getDocuments(dbName, collectionName, filterQuery, sortQuery) {
	return new Promise((resolve, reject) => {
		mongoClient.connect(BASE_URL, clientOptions, (err, client) => {
			if (err) {
				reject(err);
			} else {
				var db = client.db(dbName);
				db.collection(collectionName)
					.find(filterQuery)
					.sort(sortQuery)
					.toArray((err, documents) => {
						if (err) {
							client.close();
							reject(err);
						}
						client.close();
						resolve(documents);
					});
			}
		});
	});
}

function getDocument(dbName, collectionName, filterQuery, fieldSelection) {
	
	return new Promise((resolve, reject) => {
		mongoClient.connect(BASE_URL, clientOptions, (err, client) => {
			if (err) {
				reject(err);
			} else {
				var db = client.db(dbName);
				db.collection(collectionName)
					.findOne(filterQuery, fieldSelection)
			}
		});
	});
}

function insertDocument(dbName, collectionName, document) {
	return new Promise((resolve, reject) => {
		mongoClient.connect(BASE_URL, clientOptions, (err, client) => {
			if (err) {
				reject(err);
			} else {
				var db = client.db(dbName);
				db.collection(collectionName).insertOne(
					document,
					(err, result) => {
						if (err) {
							client.close();
							reject(err);
						}
						client.close();
						resolve(result);
					}
				);
			}
		});
	});
}

function updateDocument(dbName, collectionName, filterQuery, newDocument) {
	return new Promise((resolve, reject) => {
		mongoClient.connect(BASE_URL, clientOptions, (err, client) => {
			if (err) {
				reject(err);
			} else {
				var db = client.db(dbName);
				db.collection(collectionName).updateMany(
					filterQuery,
					JSON.stringify(newDocument).includes("$pull")
						? newDocument
						: { $set: newDocument },
					{ upsert: false },
					(err, result) => {
						if (err) {
							client.close();
							reject(err);
						}
						client.close();
						resolve(result);
					}
				);
			}
		});
	});
}

function deleteDocument(dbName, collectionName, filterQuery) {
	return new Promise((resolve, reject) => {
		mongoClient.connect(BASE_URL, clientOptions, (err, client) => {
			if (err) {
				reject(err);
			} else {
				var db = client.db(dbName);
				db.collection(collectionName).deleteMany(
					filterQuery,
					(err, result) => {
						if (err) {
							client.close();
							reject(err);
						}
						client.close();
						resolve(result);
					}
				);
			}
		});
	});
}

module.exports = {
	getOne: getDocument,
	get: getDocuments,
	insert: insertDocument,
	update: updateDocument,
	delete: deleteDocument
};
