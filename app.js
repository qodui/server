var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var fs = require("fs");
var cors = require('cors')

var sm = require("./routes/authentication");
var userMgmt = require("./routes/user.mgmt");
var capMgmt = require("./routes/cap.mgmt");
var roleMgmt = require("./routes/role.mgmt");
var environments = require("./routes/environment");
var configurations = require("./routes/configuration");
var executions = require("./routes/execution");

if (!fs.existsSync(__dirname + "/logs")) {
	fs.mkdirSync(__dirname + "/logs");
}

var accessLogStream = fs.createWriteStream(__dirname + "/logs/access.log", {
	flags: "a"
});

var app = express();

app.use(
	logger("combined", {
		stream: accessLogStream
	})
);
app.use(cors({credentials: true, origin: true}));

//app.use(express.json());
//app.use(express.urlencoded({ extended: false }));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "build")));
app.use((req, res, next) => {
//	res.header("Access-Control-Allow-Origin", "https://ec2-13-127-193-4.ap-south-1.compute.amazonaws.com:8080");
	res.header("Access-Control-Allow-Origin", req.headers.origin);
	res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
	res.header(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept, authId, userId"
	);
	next();
});

app.get("/", function(req, res) {
	res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.get("/login", function(req, res) {
	res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.use("/sm", sm);
app.use("/userMgmt", userMgmt);
app.use("/capMgmt", capMgmt);
app.use("/roleMgmt", roleMgmt);
app.use("/environments", environments);
app.use("/configurations", configurations);
app.use("/executions", executions);
app.use(errorHandler);

app.get("/healthCheck", (req, res) => {
	res.status(200).send("Server up and running.");
});

function errorHandler(err, req, res, next) {
	res.status(500).send({
		message: `${err.custom} : ${err.name}`
	});
}

module.exports = app;
